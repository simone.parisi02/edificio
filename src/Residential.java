import java.sql.Date;

public class Residential extends Edificio {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    // --------------------------------
    public Residential(String street, String city, String state, String type, int zip, int beds, int baths, int sq__ft,
            float latitude, float longitude, Date sale_date, double price) {
        super(street, city, state, type, zip, beds, baths, sq__ft, latitude, longitude, sale_date, price);
    }

    // --------------------------------
    public Residential(Residential residential) {
        super(residential);
    }

    // --------------------------------
    public Residential(String e[]) {
        super(e);
    }
}