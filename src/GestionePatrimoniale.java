import java.io.*;
import java.math.BigDecimal;
import java.util.*;

public class GestionePatrimoniale {
	// --------------------------------
	ArrayList<Edificio> edifici = new ArrayList<Edificio>();

	// --------------------------------
	public void addEdificio(Edificio e) throws Exception {
		if (e instanceof Condo) {
			edifici.add(new Condo((Condo) e));
		} else if (e instanceof MultiFamily) {
			edifici.add(new MultiFamily((MultiFamily) e));
		} else if (e instanceof Residential) {
			edifici.add(new Residential((Residential) e));
		} else if (e instanceof Unknown) {
			edifici.add(new Unknown((Unknown) e));
		}
	}

	// --------------------------------
	public void addEdificio(String[] e) throws Exception {
		switch (e[7]) {
			case ("Condo"):
				edifici.add(new Condo(e));
				break;
			case ("Multi-Family"):
				edifici.add(new MultiFamily(e));
				break;
			case ("Residential"):
				edifici.add(new Residential(e));
				break;
			default:
				edifici.add(new Unknown(e));
				break;

		}
	}

	// --------------------------------
	public void removeEdificio(Edificio e) throws Exception {
		edifici.remove(e);
	}

	// --------------------------------
	@Override
	public String toString() {
		String s = "";
		for (Edificio e : edifici) {
			s = s.concat(e.toString() + System.lineSeparator());
		}
		return s;
	}

	// --------------------------------
	public void sortByCity() {
		edifici.sort(new SortByCity());
	}

	// --------------------------------
	public void sortByPrice() {
		edifici.sort(new SortByPrice());
	}

	// --------------------------------

	/*
	 * public void loadCSV(String fileName) throws IOException { Scanner scanner =
	 * new Scanner(new File(fileName)).useDelimiter(System.lineSeparator()); boolean
	 * firstLine = true; try { while (scanner.hasNext()) { if (!firstLine) { final
	 * String[] valori = scanner.next().split(";"); addEdificio(valori); } else {
	 * firstLine = false; } } } catch (Exception e) { } finally { scanner.close(); }
	 * }
	 */

	// --------------------------------
	public void loadCSV(String fileName) throws Exception {
		String row;
		boolean firstLine = true;
		BufferedReader csvReader = new BufferedReader(new FileReader(fileName));
		while ((row = csvReader.readLine()) != null) {
			if (!firstLine) {
				String[] data = row.split(";");
				addEdificio(data);
			} else {
				firstLine = false;
			}
		}
		csvReader.close();
	}

	// --------------------------------
	public void saveBin(String fileName) throws FileNotFoundException, IOException {
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
		out.writeObject(edifici);
		out.close();
	}

	// --------------------------------
	@SuppressWarnings("unchecked")
	public void loadBin(String fileName) throws ClassNotFoundException, IOException {
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
		edifici = (ArrayList<Edificio>) in.readObject();
		in.close();
	}

	// --------------------------------
	/*
	 * public long totalPrice() { return edifici.stream().mapToLong(e -> (long)
	 * e.getPrice()).sum(); }
	 */

	// --------------------------------
	public BigDecimal totalPrice() {
		return new BigDecimal(edifici.stream().mapToDouble(e -> e.getPrice()).sum());
	}

	// --------------------------------
	public Map<String, BigDecimal> totalPriceByType() {
		Map<String, BigDecimal> dict = new HashMap<String, BigDecimal>();
		for (Edificio e : edifici) {
			String tmpKey = e.getType();
			boolean match = false;
			for (String s : dict.keySet()) {
				if (s.matches(tmpKey)) {
					match = true;
				}
			}
			if (match) {
				dict.put(tmpKey, dict.get(tmpKey).add(new BigDecimal(e.getPrice())));
			} else {
				dict.put(tmpKey, new BigDecimal(e.getPrice()));
			}
		}
		return dict;
	}
}
