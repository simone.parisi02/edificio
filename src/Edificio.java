import java.io.Serializable;
import java.util.Date;
import java.text.SimpleDateFormat;

public abstract class Edificio implements Serializable {
    // --------------------------------
    private static final long serialVersionUID = 1L;
    // --------------------------------
    private String street, city, state, type;
    private int zip, beds, baths, sq__ft;
    private float latitude, longitude;
    private Date sale_date;
    private double price;

    // --------------------------------
    public Edificio(String street, String city, String state, String type, int zip, int beds, int baths, int sq__ft,
            float latitude, float longitude, Date sale_date, double price) {
        this.street = street;
        this.city = city;
        this.state = state;
        this.type = type;
        this.zip = zip;
        this.beds = beds;
        this.baths = baths;
        this.sq__ft = sq__ft;
        this.latitude = latitude;
        this.longitude = longitude;
        this.sale_date = sale_date;
        this.price = price;
    }

    // --------------------------------
    public Edificio(Edificio e) {
        this.street = e.street;
        this.city = e.city;
        this.state = e.state;
        this.type = e.type;
        this.zip = e.zip;
        this.beds = e.beds;
        this.baths = e.baths;
        this.sq__ft = e.sq__ft;
        this.latitude = e.latitude;
        this.longitude = e.longitude;
        this.sale_date = e.sale_date;
        this.price = e.price;
    }

    // --------------------------------
    public Edificio(String[] e) {
        this.street = e[0];
        this.city = e[1];
        this.zip = Integer.valueOf(e[2]);
        this.state = e[3];
        this.beds = Integer.valueOf(e[4]);
        this.baths = Integer.valueOf(e[5]);
        this.sq__ft = Integer.valueOf(e[6]);
        this.type = e[7];

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy");
            this.sale_date = formatter.parse(e[8]);
        } catch (Exception ignore) {
            this.sale_date = null;
        }

        this.price = Double.valueOf(e[9]);
        this.latitude = Float.valueOf(e[10]);
        this.longitude = Float.valueOf(e[11]);

    }

    // --------------------------------
    public String getStreet() {
        return this.street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getZip() {
        return this.zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public int getBeds() {
        return this.beds;
    }

    public void setBeds(int beds) {
        this.beds = beds;
    }

    public int getBaths() {
        return this.baths;
    }

    public void setBaths(int baths) {
        this.baths = baths;
    }

    public int getSq__ft() {
        return this.sq__ft;
    }

    public void setSq__ft(int sq__ft) {
        this.sq__ft = sq__ft;
    }

    public float getLatitude() {
        return this.latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return this.longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public Date getSale_date() {
        return this.sale_date;
    }

    public void setSale_date(Date sale_date) {
        this.sale_date = sale_date;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    // --------------------------------
    @Override
    public String toString() {
        return street + ";" + city + ";" + zip + ";" + state + ";" + beds + ";" + baths + ";" + sq__ft + ";" + type
                + ";" + sale_date + ";" + price + ";" + latitude + ";" + longitude;
    }

    // --------------------------------
    @Override
    public boolean equals(Object o) {
        return (o instanceof Edificio) ? (this.toString().matches(o.toString())) : false;
    }

}