import java.util.Comparator;

// --------------------------------
class SortByCity implements Comparator<Edificio> {
    public int compare(Edificio a, Edificio b) {
        return a.getCity().compareTo(b.getCity());
    }

}

// --------------------------------
class SortByPrice implements Comparator<Edificio> {
    public int compare(Edificio a, Edificio b) {
        return (int) (a.getPrice() - b.getPrice());
    }
}
// --------------------------------
