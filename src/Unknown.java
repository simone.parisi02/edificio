import java.sql.Date;

public class Unknown extends Edificio {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    // --------------------------------
    public Unknown(String street, String city, String state, String type, int zip, int beds, int baths, int sq__ft,
            float latitude, float longitude, Date sale_date, double price) {
        super(street, city, state, type, zip, beds, baths, sq__ft, latitude, longitude, sale_date, price);
    }

    // --------------------------------
    public Unknown(Unknown unknown) {
        super(unknown);
    }

    // --------------------------------
    public Unknown(String e[]) {
        super(e);
    }

}